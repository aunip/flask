# All U Need Is Pizza

> _Made With **Python** 3_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- controllers
    +-- __init.py__
    +-- pizza_ctrl.py
+-- models
    +-- __init.py__
    +-- pizza_model.py
+-- public
    +-- favicon.png
    +-- index.html
    +-- oauth2*.html
    +-- swagger-ui*.{js,css}
    +-- swagger.json
+-- services
    +-- __init.py__
    +-- pizza_service.py
+-- .gitignore
+-- LICENSE
+-- main.py
+-- pizzas.json
+-- README.md
+-- requirements.txt
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/python.git
```

Init:

```
python -m venv env
source env/bin/activate
```

Install:

```
pip install -r requirements.txt
```

Launch:

```
python main.py
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
