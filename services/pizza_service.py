from bson import ObjectId
from json import load
from models import IPizza as PizzaModel


def init_pizzas():
    """
    Initialize Pizzas
    Read JSON File & Feed DB
    """
    with open('pizzas.json') as json_file:
        pizzas = load(json_file)
        count = read_all_pizzas()

        if len(count) == 0:
            add_many_pizzas(pizzas)

        else:
            for p in pizzas:
                pizza = read_pizza_by_label(p['label'])

                if not pizza:
                    add_pizza(p)

                else:
                    modify_pizza(
                        pizza.id, {'items': p['items'], 'price': p['price']})

                    # modify_pizza(
                    #     pizza.id, items=p['items'], price=p['price'])


def add_many_pizzas(pizzas):
    """
    Add Many Pizzas

    :param pizzas: Pizzas
    :type pizzas: list
    """
    try:
        for p in pizzas:
            pizza = PizzaModel(
                label=p['label'], items=p['items'], price=p['price'])
            pizza.save()
    except:
        return None

    return {'createdCount': len(pizzas)}


def add_pizza(data):
    """
    Add Pizza

    :param data: Data
    :type data: object
    """
    try:
        pizza = PizzaModel(
            label=data['label'], items=data['items'], price=data['price'])
        pizza.save()
    except:
        return None

    return {'createdId': str(pizza.id)}


def read_all_pizzas():
    """
    Read All Pizzas
    """
    try:
        pizzas = PizzaModel.objects()
    except:
        return None

    return pizzas


def read_pizza(id):
    """
    Read Pizza

    :param id: ID
    :type id: str
    """
    try:
        pizza = PizzaModel.objects(id=ObjectId(id)).get()
    except:
        return None

    return pizza


def read_pizza_by_label(label):
    """
    Read Pizza By Label

    :param label: Label
    :type label: str
    """
    try:
        pizza = PizzaModel.objects(label=label).get()
    except:
        return None

    return pizza


def modify_pizza(id, data):
    """
    Modify Pizza

    :param id: ID
    :type id: str
    :param data: Data
    :type data: object
    """
    try:
        pizza = PizzaModel.objects(id=ObjectId(id)).get()
        pizza.update(**data)
    except:
        return None

    return {'updatedId': str(pizza.id)}


def delete_all_pizzas():
    """
    Delete All Pizzas
    """
    try:
        pizzas = PizzaModel.objects()

        for p in pizzas:
            p.delete()
    except:
        return None

    return {'deletedCount': len(pizzas)}


def delete_pizza(id):
    """
    Delete Pizza

    :param id: ID
    :type id: str
    """
    try:
        pizza = PizzaModel.objects(id=ObjectId(id)).get()
        pizza.delete()
    except:
        return None

    return {'deletedId': str(pizza.id)}
