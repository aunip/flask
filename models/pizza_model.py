from mongoengine import Document
from mongoengine.fields import ObjectIdField, StringField, ListField, FloatField
from bson import ObjectId


class Pizza:
    def __init__(self, id, label, items, price):
        self.__id = str(id)
        self.__label = label
        self.__items = items
        self.__price = price

    def get_id(self):
        return self.__id

    def get_label(self):
        return self.__label

    def get_items(self):
        return self.__items

    def get_price(self):
        return self.__price

    def to_dict(self):
        return {'id': self.__id, 'label': self.__label, 'items': self.__items, 'price': self.__price}


class IPizza(Document):
    meta = {'collection': 'pizzas'}
    id = ObjectIdField(name='_id', default=ObjectId, primary_key=True)
    label = StringField()
    items = ListField(StringField())
    price = FloatField()
