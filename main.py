from mongoengine import connect
from flask import Flask, send_from_directory
from controllers import controllers as pizza_ctrl
from services import init_pizzas

connect('aunip', host='localhost', port=27017)

app = Flask(__name__, static_url_path='', static_folder='public', template_folder='public')

app.register_blueprint(pizza_ctrl, url_prefix='/api')

PORT = 5050


@app.route('/api-docs')
def api_docs():
    return send_from_directory('public', 'index.html')


if __name__ == '__main__':
    init_pizzas()
    print("> Listening On 'http://localhost:" + str(PORT) + "'")
    app.run(host='localhost', port=PORT)
