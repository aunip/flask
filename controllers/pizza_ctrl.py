from flask import Blueprint, Response, request, make_response, jsonify
from models import Pizza
from services import (
    add_many_pizzas,
    add_pizza,
    read_all_pizzas,
    read_pizza,
    modify_pizza,
    delete_all_pizzas,
    delete_pizza
)

controllers = Blueprint('pizza_ctrl', __name__)


@controllers.route('/pizzas', methods=['POST'])
def create_many_pizzas():
    """
    Create Many Pizzas (From Array)

    :return: Response
    :rtype: object
    """
    body = request.get_json()

    if len(body) == 0:
        return Response(status=400)

    result = add_many_pizzas(body)

    if result == None:
        return Response(status=500)

    return make_response(result, 201)


@controllers.route('/pizza', methods=['POST'])
def create_pizza():
    """
    Create Pizza (From Object)

    :return: Response
    :rtype: object
    """
    body = request.get_json()

    if not body.get('label') or not body.get('items') or not body.get('price'):
        return Response(status=400)

    result = add_pizza(body)

    if result == None:
        return Response(status=500)

    return make_response(jsonify(result), 201)


@controllers.route('/pizzas', methods=['GET'])
def get_all_pizzas():
    """
    Get All Pizzas

    :return: Response
    :rtype: list
    """
    result = read_all_pizzas()

    if result == None:
        return Response(status=500)

    results = []

    for res in result:
        pizza = Pizza(res.id, res.label, res.items, res.price)
        results.append(pizza.to_dict())

    return make_response(jsonify(results), 200)


@controllers.route('/pizza/<id>', methods=['GET'])
def get_pizza(id):
    """
    Get Pizza

    :param id: ID
    :type id: str
    :return: Response
    :rtype: object
    """
    if not id:
        return Response(status=400)

    res = read_pizza(id)

    if res == None:
        return Response(status=404)

    result = Pizza(res.id, res.label, res.items, res.price)

    return make_response(jsonify(result.to_dict()), 200)


@controllers.route('/pizza/<id>', methods=['PUT'])
def update_pizza(id):
    """
    Update Pizza

    :param id: ID
    :type id: str
    :return: Response
    :rtype: object
    """
    if not id:
        return Response(status=400)

    body = request.get_json()
    result = modify_pizza(id, body)

    if result == None:
        return Response(status=404)

    return make_response(jsonify(result), 200)


@controllers.route('/pizzas', methods=['DELETE'])
def clear_all_pizzas():
    """
    Clear All Pizzas

    :return: Response
    :rtype: object
    """
    result = delete_all_pizzas()

    if result == None:
        return Response(status=500)

    return make_response(jsonify(result), 200)


@controllers.route('/pizza/<id>', methods=['DELETE'])
def clear_pizza(id):
    """
    Clear Pizza

    :param id: ID
    :type id: str
    :return: Response
    :rtype: object
    """
    if not id:
        return Response(status=400)

    result = delete_pizza(id)

    if result == None:
        return Response(status=404)

    return make_response(jsonify(result), 200)
